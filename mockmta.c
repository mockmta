/* mockmta - mock MTA server for use in test suites
 * Copyright (C) 2020-2021 Sergey Poznyakoff
 *
 * Mockmta is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Mockmta is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU Mailutils.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <syslog.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include <ctype.h>
#ifdef HAVE_PATHS_H
# include <paths.h>
#endif
#ifndef _PATH_DEVNULL
# define _PATH_DEVNULL "/dev/null"
#endif
#ifdef WITH_LIBWRAP
# include <tcpd.h>
#endif

char *progname;
char *mailbox_name;
int daemon_opt;
int smtp_timeout = 5*60;
static char *portstr = "25";
int log_level = LOG_INFO;
char *pidfile;

enum
  {
    SMTP_IN,
    SMTP_OUT
  };

void smtp_timer_enqueue (pthread_t tid, int state);
void smtp_timer_dequeue (pthread_t tid);

enum
  {
    EX_OK,
    EX_FAILURE,
    EX_TEMPFAIL,
    EX_USAGE
  };


#define SID_ABC "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define SID_ABC_LEN (sizeof (SID_ABC) - 1)
#define SID_ABC_LEN_SQR (SID_ABC_LEN * SID_ABC_LEN)

#define SID_BUF_LEN 9

/*
 * Generate (almost) unique session ID.  The idea borrowed from Sendmail
 * queue ID.
 */
int
generate_sid (char *sidbuf)
{
  static int init;
  static unsigned int seqno = 0;
  static pthread_mutex_t sid_mutex = PTHREAD_MUTEX_INITIALIZER;
  static char abc[] = SID_ABC;
  
  struct timeval t;
  struct tm *tm;
  unsigned int n;
  char *p;

  gettimeofday (&t, NULL);

  pthread_mutex_lock (&sid_mutex);
  if (!init)
    {
      seqno = getpid () + t.tv_sec + t.tv_usec;
      init = 1;
    }
  
  n = seqno++ % SID_ABC_LEN_SQR;
  pthread_mutex_unlock (&sid_mutex);
  
  tm = gmtime (&t.tv_sec);
  sidbuf[0] = abc[tm->tm_year % SID_ABC_LEN];
  sidbuf[1] = abc[tm->tm_mon];
  sidbuf[2] = abc[tm->tm_mday];
  sidbuf[3] = abc[tm->tm_hour];
  sidbuf[4] = abc[tm->tm_min % SID_ABC_LEN];
  sidbuf[5] = abc[tm->tm_sec % SID_ABC_LEN];
  sidbuf[6] = abc[n / SID_ABC_LEN];
  sidbuf[7] = abc[n % SID_ABC_LEN];
  sidbuf[8] = 0;

  p = strdup (sidbuf);
  if (!p)
    return errno;
  return 0;
}

static void (*logger) (int, char const *fmt, ...);

static pthread_key_t sdat_key;
static pthread_once_t sdat_key_once = PTHREAD_ONCE_INIT;

struct session_data
{
  char sid[SID_BUF_LEN];
  int nomem;
  char *fmtbuf;
  size_t fmtsize;
};

static void
sdat_free (void *f)
{
  free (f);
}

static void
make_sdat_key (void)
{
  pthread_key_create (&sdat_key, sdat_free);
}

static struct session_data *
priv_get_session_data (void)
{
  struct session_data *sdat;
  pthread_once (&sdat_key_once, make_sdat_key);
  if ((sdat = pthread_getspecific (sdat_key)) == NULL)
    {
      sdat = calloc (1, sizeof(*sdat));
      if (sdat == NULL)
	logger (LOG_CRIT, "out of memory");
      pthread_setspecific (sdat_key, sdat);
    }
  return sdat;
}

static void nomemory (void);

static void
log_stderr (int prio, char const *fmt, ...)
{
  va_list ap;
  int m;
  int ec = errno;
  char const *es = NULL;
  size_t len;
  struct session_data *sd = priv_get_session_data ();

  if (prio > log_level)
    return;
  
  if (!sd->nomem)
    {
      for (m = 0; fmt[m += strcspn (fmt + m, "%")]; )
	{
	  m++;
	  if (fmt[m] == 'm')
	    break;
	}      
      
      len = strlen (fmt) + 1;
      if (fmt[m])
	{
	  es = strerror (ec);
	  len += strlen (es) - 2;
	}

      if (es)
	{
	  if (len > sd->fmtsize)
	    {
	      sd->fmtsize = len;
	      sd->fmtbuf = realloc (sd->fmtbuf, sd->fmtsize);
	      if (!sd->fmtbuf)
		nomemory ();
	    }

	  memcpy (sd->fmtbuf, fmt, m - 1);
	  memcpy (sd->fmtbuf + m - 1, es, strlen (es) + 1);
	  strcat (sd->fmtbuf, fmt + m + 1);

	  fmt = sd->fmtbuf;
	}
    }
  
  va_start (ap, fmt);
  fprintf (stderr, "%s: ", progname);
  if (sd->sid[0])
    fprintf (stderr, "%s: ", sd->sid);
  vfprintf (stderr, fmt, ap);
  fputc ('\n', stderr);
  va_end (ap);
}

static void
log_syslog (int prio, char const *fmt, ...)
{
  va_list ap;
  struct session_data *sd = priv_get_session_data ();

  if (prio > log_level)
    return;

  if (!sd->nomem && sd->sid[0])
    {
      size_t len = strlen (sd->sid) + 2 + strlen (fmt) + 1;
      if (len > sd->fmtsize)
	{
	  sd->fmtsize = len;
	  sd->fmtbuf = realloc (sd->fmtbuf, sd->fmtsize);
	  if (!sd->fmtbuf)
	    nomemory ();
	}
      snprintf (sd->fmtbuf, sd->fmtsize, "%s: %s", sd->sid, fmt);
      fmt = sd->fmtbuf;
    }

  va_start (ap, fmt);
  vsyslog (prio, fmt, ap);
  va_end (ap);
}

static void (*logger) (int, char const *fmt, ...) = log_stderr;

static void
nomemory (void)
{
  struct session_data *sd = priv_get_session_data ();
  sd->nomem = 1;
  logger (LOG_CRIT, "out of memory");
  exit (EX_FAILURE);
}

static char *
sockaddr_str (struct sockaddr *sa, int salen)
{
  char buf[512];

  switch (sa->sa_family)
    {
    case AF_INET:
    case AF_INET6:
      {
	char host[NI_MAXHOST];
	char srv[NI_MAXSERV];

	if (getnameinfo (sa, salen,
			 host, sizeof (host), srv, sizeof (srv),
			 NI_NUMERICHOST|NI_NUMERICSERV) == 0)
	  snprintf (buf, sizeof buf, "%s://%s:%s",
		    sa->sa_family == AF_INET ? "inet" : "inet6",
		    host, srv);
	else
	  snprintf (buf, sizeof buf, "%s://[getnameinfo failed]",
		    sa->sa_family == AF_INET ? "inet" : "inet6");
	break;
      }

    default:
      /* Should not happen */
      snprintf (buf, sizeof buf, "family:%d", sa->sa_family);
    }
  return strdup (buf);
}

static int 
is_numstr (const char *p)
{
  if (!*p)
    return 0;
  for (; *p && isascii (*p) && isdigit (*p);p++)
    ;
  return *p == 0;
}

struct addrinfo *
address_parse (char const *addrstr)
{
  struct addrinfo hints;
  struct addrinfo *res;
  int rc;
  char const *node;
  char const *service;
  char *nodebuf = NULL;
  
  memset (&hints, 0, sizeof (hints));
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_family = AF_INET;

  service = strrchr (addrstr, ':');
  if (service)
    {
      size_t len = service - addrstr;

      if (len == 0)
	node = NULL;
      else
	{
	  nodebuf = malloc (len + 1);
	  if (!nodebuf)
	    nomemory ();
	  memcpy (nodebuf, addrstr, len);
	  nodebuf[len] = 0;
	  node = nodebuf;
	}
      service++;
      if (!*service)
	{
	  service = "25";
	  hints.ai_flags |= AI_NUMERICSERV;
	}
    }
  else if (is_numstr (addrstr))
    {
      node = "127.0.0.1";
      service = addrstr;
      hints.ai_flags |= AI_NUMERICSERV;
    }
  else
    {
      node = addrstr;
      service = "25";
      hints.ai_flags |= AI_NUMERICSERV;		
    }
	
  rc = getaddrinfo (node, service, &hints, &res);
  free (nodebuf);

  switch (rc)
    {
    case 0:
      break;
      
    case EAI_SYSTEM:
      logger (LOG_CRIT, "cannot parse address: %m");
      exit (EX_USAGE);
      
    case EAI_BADFLAGS:
    case EAI_SOCKTYPE:
      logger (LOG_CRIT, "%s:%d: internal error converting address",
	      __FILE__,__LINE__);
      exit (EX_FAILURE);
      
    case EAI_MEMORY:
      nomemory ();
      
    default:
      logger (LOG_ERR, "getaddrinfo: %s", gai_strerror (rc));
      exit (EX_FAILURE);      
    }
  return res;
}

struct iodrv
{
  int (*drv_read) (void *, char *, size_t, size_t *);
  int (*drv_write) (void *, char *, size_t, size_t *);
  void (*drv_close) (void *);
  const char *(*drv_strerror) (void *, int);
};

#define IOBUFSIZE 1024

struct iobase
{
  struct iodrv *iob_drv;
  char iob_buf[IOBUFSIZE];
  size_t iob_start;
  size_t iob_level;
  int iob_errno;
  int iob_eof;
};

static struct iobase *
iobase_create (struct iodrv *drv, size_t size)
{
  struct iobase *bp;

  bp = calloc (1, size);
  if (!bp)
    nomemory ();

  bp->iob_drv = drv;
  bp->iob_start = 0;
  bp->iob_level = 0;
  bp->iob_errno = 0;
  bp->iob_eof = 0;

  return bp;
}

/* Number of data bytes in buffer */
static inline size_t
iobase_data_bytes (struct iobase *bp)
{
  return bp->iob_level - bp->iob_start;
}

/* Pointer to the first data byte */
static inline char *
iobase_data_start (struct iobase *bp)
{
  return bp->iob_buf + bp->iob_start;
}

static inline void
iobase_data_less (struct iobase *bp, size_t n)
{
  bp->iob_start += n;
  if (bp->iob_start == bp->iob_level)
    {
      bp->iob_start = 0;
      bp->iob_level = 0;
    }
}

static inline int
iobase_data_getc (struct iobase *bp)
{
  char *p;
  if (iobase_data_bytes (bp) == 0)
    return -1;
  p = iobase_data_start (bp);
  iobase_data_less (bp, 1);
  return *p;
}

static inline int
iobase_data_ungetc (struct iobase *bp, int c)
{
  if (bp->iob_start > 0)
    bp->iob_buf[--bp->iob_start] = c;
  else if (bp->iob_level == 0)
    bp->iob_buf[bp->iob_level++] = c;
  else
    return -1;
  return 0;
}

/* Number of bytes available for writing in buffer */
static inline size_t
iobase_avail_bytes (struct iobase *bp)
{
  return IOBUFSIZE - bp->iob_level;
}

/* Pointer to the first byte available for writing */
static inline char *
iobase_avail_start (struct iobase *bp)
{
  return bp->iob_buf + bp->iob_level;
}

static inline void
iobase_avail_less (struct iobase *bp, size_t n)
{
  bp->iob_level += n;
}

/* Fill the buffer */
static inline int
iobase_fill (struct iobase *bp)
{
  int rc;
  size_t n;

  rc = bp->iob_drv->drv_read (bp, iobase_avail_start (bp),
			      iobase_avail_bytes (bp), &n);
  if (rc == 0)
    {
      if (n == 0)
	bp->iob_eof = 1;
      else
	iobase_avail_less (bp, n);
    }
  bp->iob_errno = rc;
  return rc;
}

/* Flush the data available in buffer to external storage */
static inline int
iobase_flush (struct iobase *bp)
{
  int rc;
  size_t n;

  rc = bp->iob_drv->drv_write (bp, iobase_data_start (bp),
			       iobase_data_bytes (bp), &n);
  if (rc == 0)
    iobase_data_less (bp, n);
  bp->iob_errno = rc;
  return rc;
}

static inline char const *
iobase_strerror (struct iobase *bp)
{
  return bp->iob_drv->drv_strerror (bp, bp->iob_errno);
}

static inline int
iobase_eof (struct iobase *bp)
{
  return bp->iob_eof && iobase_data_bytes (bp) == 0;
}

#if 0
/* Not actually used.  Provided for completeness sake. */
static ssize_t
iobase_read (struct iobase *bp, char *buf, size_t size)
{
  size_t len = 0;

  while (size)
    {
      size_t n = iobase_data_bytes (bp);
      if (n == 0)
	{
	  if (bp->iob_eof)
	    break;
	  if (iobase_fill (bp))
	    break;
	  continue;
	}
      if (n > size)
	n = size;
      memcpy (buf, iobase_data_start (bp), n);
      iobase_data_less (bp, n);
      len += n;
      buf += n;
      size -= n;
    }

  if (len == 0 && bp->iob_errno)
    return -1;
  return len;
}
#endif

static ssize_t
iobase_readln (struct iobase *bp, char *buf, size_t size)
{
  size_t len = 0;
  int cr_seen = 0;

  size--;
  while (len < size)
    {
      int c = iobase_data_getc (bp);
      if (c < 0)
	{
	  if (bp->iob_eof)
	    break;
	  if (iobase_fill (bp))
	    break;
	  continue;
	}
      if (c == '\r')
	{
	  cr_seen = 1;
	  continue;
	}
      if (c != '\n' && cr_seen)
	{
	  buf[len++] = '\r';
	  if (len == size)
	    {
	      if (iobase_data_ungetc (bp, c))
		abort ();
	      break;
	    }
	}
      cr_seen = 0;
      buf[len++] = c;
      if (c == '\n')
	break;
    }
  if (len == 0 && bp->iob_errno)
    return -1;
  buf[len] = 0;
  return len;
}

static ssize_t
iobase_write (struct iobase *bp, char *buf, size_t size)
{
  size_t len = 0;

  while (size)
    {
      size_t n = iobase_avail_bytes (bp);
      if (n == 0)
	{
	  if (iobase_flush (bp))
	    break;
	  continue;
	}
      if (n > size)
	n = size;
      memcpy (iobase_avail_start (bp), buf + len, n);
      iobase_avail_less (bp, n);
      len += n;
      size -= n;
    }
  if (len == 0 && bp->iob_errno)
    return -1;
  return len;
}

static ssize_t
iobase_writeln (struct iobase *bp, char *buf, size_t size)
{
  size_t len = 0;

  while (size)
    {
      char *p = memchr (buf, '\n', size);
      size_t n = p ? p - buf + 1 : size;
      ssize_t rc = iobase_write (bp, buf, n);
      if (rc <= 0)
	break;
      if (p && iobase_flush (bp))
	break;
      buf = p;
      len += rc;
      size -= rc;
    }
  if (len == 0 && bp->iob_errno)
    return -1;
  return len;
}

static void
iobase_close (struct iobase *bp)
{
  bp->iob_drv->drv_close (bp);
  free (bp);
}

/* File-descriptor I/O streams */
struct iofile
{
  struct iobase base;
  int fd;
};

static int
iofile_read (void *sd, char *data, size_t size, size_t *nbytes)
{
  struct iofile *bp = sd;
  ssize_t n = read (bp->fd, data, size);
  if (n == -1)
    return errno;
  if (nbytes)
    *nbytes = n;
  return 0;
}

static int
iofile_write (void *sd, char *data, size_t size, size_t *nbytes)
{
  struct iofile *bp = sd;
  ssize_t n = write (bp->fd, data, size);
  if (n == -1)
    return errno;
  if (nbytes)
    *nbytes = n;
  return 0;
}

static const char *
iofile_strerror (void *sd, int rc)
{
  return strerror (rc);
}

static void
iofile_close (void *sd)
{
  struct iofile *bp = sd;
  close (bp->fd);
}

static struct iodrv iofile_drv = {
  iofile_read,
  iofile_write,
  iofile_close,
  iofile_strerror
};

struct iobase *
iofile_create (int fd)
{
  struct iofile *bp = (struct iofile *) iobase_create (&iofile_drv, sizeof (*bp));
  bp->fd = fd;
  return (struct iobase*) bp;
}

enum
  {
    IO2_RD,
    IO2_WR
  };

static void disable_starttls (void);

#ifdef WITH_TLS
# include <gnutls/gnutls.h>

/* TLS support */
char *tls_cert;			/* TLS sertificate */
char *tls_key;			/* TLS key */
char *tls_cafile;

static inline int
set_tls_opt (int c)
{
  switch (c)
    {
    case 'a':
      tls_cafile = optarg;
      break;

    case 'c':
      tls_cert = optarg;
      break;
      
    case 'k':
      tls_key = optarg;
      break;
      
    default:
      return 1;
    }
  return 0;
}

static inline int
enable_tls (void)
{
  return tls_cert != NULL && tls_key != NULL;
}

/* TLS streams */
struct iotls
{
  struct iobase base;
  gnutls_session_t sess;
  int fd[2];
};

static int
iotls_read (void *sd, char *data, size_t size, size_t *nbytes)
{
  struct iotls *iob = sd;
  int rc;
  
  do
    rc = gnutls_record_recv (iob->sess, data, size);
  while (rc == GNUTLS_E_AGAIN || rc == GNUTLS_E_INTERRUPTED);
  if (rc >= 0)
    {
      if (nbytes)
	*nbytes = rc;
      return 0;
    }
  return rc;  
}

static int
iotls_write (void *sd, char *data, size_t size, size_t *nbytes)
{
  struct iotls *iob = sd;
  int rc;

  do
    rc = gnutls_record_send (iob->sess, data, size);
  while (rc == GNUTLS_E_INTERRUPTED || rc == GNUTLS_E_AGAIN);
  if (rc >= 0)
    {
      if (nbytes)
	*nbytes = rc;
      return 0;
    }
  return rc;
}

static const char *
iotls_strerror (void *sd, int rc)
{
  //  struct iotls *iob = sd;
  return gnutls_strerror (rc);
}

static void
iotls_close (void *sd)
{
  struct iotls *iob = sd;
  gnutls_bye (iob->sess, GNUTLS_SHUT_RDWR);
  gnutls_deinit (iob->sess);
}

static struct iodrv iotls_drv = {
  iotls_read,
  iotls_write,
  iotls_close,
  iotls_strerror
};

static gnutls_dh_params_t dh_params;
static gnutls_certificate_server_credentials x509_cred;

static void
_tls_cleanup_x509 (void)
{
  if (x509_cred)
    gnutls_certificate_free_credentials (x509_cred);
}

#define DH_BITS 512
static void
generate_dh_params (void)
{
  gnutls_dh_params_init (&dh_params);
  gnutls_dh_params_generate2 (dh_params, DH_BITS);
}

static int
tls_init (void)
{
  int rc;
  
  if (!enable_tls())
    return -1;

  gnutls_global_init ();
  atexit (gnutls_global_deinit);
  gnutls_certificate_allocate_credentials (&x509_cred);
  atexit (_tls_cleanup_x509);
  if (tls_cafile)
    {
      rc = gnutls_certificate_set_x509_trust_file (x509_cred,
						   tls_cafile,
						   GNUTLS_X509_FMT_PEM);
      if (rc < 0)
	{
	  logger (LOG_ERR, "%s: %s", tls_cafile, gnutls_strerror (rc));
	  return -1;
	}
    }

  rc = gnutls_certificate_set_x509_key_file (x509_cred,
					     tls_cert, tls_key,
					     GNUTLS_X509_FMT_PEM);
  if (rc < 0)
    {
      logger (LOG_ERR, "error reading certificate files: %s",
	      gnutls_strerror (rc));
      return -1;
    }
  
  generate_dh_params ();
  gnutls_certificate_set_dh_params (x509_cred, dh_params);

  return 0;
}

static ssize_t
_tls_fd_pull (gnutls_transport_ptr_t fd, void *buf, size_t size)
{
  struct iotls *bp = fd;
  int rc;
  do
    {
      rc = read (bp->fd[IO2_RD], buf, size);
    }
  while (rc == -1 && errno == EAGAIN);
  return rc;
}

static ssize_t
_tls_fd_push (gnutls_transport_ptr_t fd, const void *buf, size_t size)
{
  struct iotls *bp = fd;
  int rc;
  do
    {
      rc = write (bp->fd[IO2_WR], buf, size);
    }
  while (rc == -1 && errno == EAGAIN);
  return rc;
}

struct iobase *
iotls_create (int in, int out)
{
  struct iotls *bp = (struct iotls *) iobase_create (&iotls_drv, sizeof (*bp));
  int rc;

  bp->fd[IO2_RD] = in;
  bp->fd[IO2_WR] = out;
  gnutls_init (&bp->sess, GNUTLS_SERVER);
  gnutls_set_default_priority (bp->sess);
  gnutls_credentials_set (bp->sess, GNUTLS_CRD_CERTIFICATE, x509_cred);
  gnutls_certificate_server_set_request (bp->sess, GNUTLS_CERT_REQUEST);
  gnutls_dh_set_prime_bits (bp->sess, DH_BITS);

  gnutls_transport_set_pull_function (bp->sess, _tls_fd_pull);
  gnutls_transport_set_push_function (bp->sess, _tls_fd_push);

  gnutls_transport_set_ptr2 (bp->sess,
			     (gnutls_transport_ptr_t) bp,
			     (gnutls_transport_ptr_t) bp);
  rc = gnutls_handshake (bp->sess);
  if (rc < 0)
    {
      gnutls_deinit (bp->sess);
      gnutls_perror (rc);
      free (bp);
      return NULL;
    }

  return (struct iobase *)bp;
}
#else
static inline int set_tls_opt (int c) {
  logger (LOG_ERR,
	  "option -%c not supported: program compiled without support for TLS",
	  c);
  return 1;
}
static inline int enable_tls(void) { return 0; }
static inline int tls_init (void) { return -1; }
static inline struct iobase *iotls_create (int in, int out) { return NULL; }
#endif

/* Two-way I/O */
struct io2
{
  struct iobase base;
  struct iobase *iob[2];
};

static int
io2_read (void *sd, char *data, size_t size, size_t *nbytes)
{
  struct io2 *iob = sd;
  ssize_t n = iobase_readln (iob->iob[IO2_RD], data, size);
  if (n < 0)
    return -(1 + IO2_RD);
  *nbytes = n;
  return 0;
}

static int
io2_write (void *sd, char *data, size_t size, size_t *nbytes)
{
  struct io2 *iob = sd;
  ssize_t n = iobase_writeln (iob->iob[IO2_WR], data, size);
  if (n < 0)
    return -(1 + IO2_WR);
  *nbytes = n;
  return 0;
}

static char const *
io2_strerror (void *sd, int rc)
{
  struct io2 *iob = sd;
  int n = -rc - 1;
  switch (n)
    {
    case IO2_RD:
    case IO2_WR:
      return iobase_strerror (iob->iob[n]);

    default:
      return "undefined error";
    }
}

static void
io2_close (void *sd)
{
  struct io2 *iob = sd;
  iobase_close (iob->iob[IO2_RD]);  
  iobase_close (iob->iob[IO2_WR]);
}

static struct iodrv io2_drv = {
  io2_read,
  io2_write,
  io2_close,
  io2_strerror
};

struct iobase *
io2_create (struct iobase *in, struct iobase *out)
{
  struct io2 *bp = (struct io2 *) iobase_create (&io2_drv, sizeof (*bp));
  bp->iob[IO2_RD] = in;
  bp->iob[IO2_WR] = out;
  return (struct iobase*) bp;
}

/* SMTP implementation */
enum smtp_state
  {
    STATE_ERR, // Reserved
    STATE_INIT,
    STATE_EHLO,
    STATE_MAIL,
    STATE_RCPT,
    STATE_DATA,
    STATE_QUIT,
    MAX_STATE
  };

#define MAX_RCPT 32

static char *state_str[] = {
  [STATE_ERR] = "error",
  [STATE_INIT] = "init",
  [STATE_EHLO] = "ehlo",
  [STATE_MAIL] = "mail",
  [STATE_RCPT] = "rcpt",
  [STATE_DATA] = "data",
  [STATE_QUIT] = "quit"
};

struct smtp
{
  enum smtp_state state;
  struct iobase *iob;
  char sid[SID_BUF_LEN];
  char buf[IOBUFSIZE];
  char *arg;
  int capa_mask;
  char *helo;
  char *sender;
  char *rcpt[MAX_RCPT];
  int nrcpt;
  int delivered;
};

enum
  {
    CAPA_PIPELINING,
    CAPA_STARTTLS,
    CAPA_HELP,
    MAX_CAPA
  };

static char const *capa_str[] = {
  "PIPELINING",
  "STARTTLS",
  "HELP"
};

#define CAPA_MASK(n) (1<<(n))

struct smtp *
smtp_create (int ifd, int ofd)
{
  struct iobase *iob = io2_create (iofile_create (ifd), iofile_create (ofd));
  struct smtp *smtp = calloc(1, sizeof (*smtp));
  if (!smtp)
    nomemory ();
  smtp->state = STATE_INIT;
  smtp->iob = iob;
  smtp->capa_mask = 0;
  if (!enable_tls ())
    smtp->capa_mask |= CAPA_MASK (CAPA_STARTTLS);
  smtp->helo = NULL;
  smtp->sender = NULL;
  smtp->nrcpt = 0;
  generate_sid (smtp->sid);
  return smtp;
}

static ssize_t
smtp_io_readln (struct smtp *smtp)
{
  ssize_t n;
  smtp_timer_enqueue (pthread_self (), SMTP_IN);
  n = iobase_readln (smtp->iob, smtp->buf, sizeof (smtp->buf));
  smtp_timer_dequeue (pthread_self ());
  return n;
}

static void
smtp_io_send (struct smtp *smtp, int code, char *fmt, ...)
{
  va_list ap;
  char buf[IOBUFSIZE];
  int n;
  
  snprintf (buf, sizeof buf, "%3d ", code);
  va_start (ap, fmt);
  n = vsnprintf (buf + 4, sizeof buf - 6, fmt, ap);
  va_end (ap);
  n += 4;
  buf[n++] = '\r';
  buf[n++] = '\n';
  smtp_timer_enqueue (pthread_self (), SMTP_OUT);  
  if (iobase_writeln (smtp->iob, buf, n) < 0)
    {
      logger (LOG_ERR, "iobase_writeln: %s", iobase_strerror (smtp->iob));
      pthread_exit (NULL);
    }
  smtp_timer_dequeue (pthread_self ());  
  logger (LOG_DEBUG, "S: %.*s", n-2, buf);
}

static void
smtp_io_mlsend (struct smtp *smtp, int code, char const **av)
{
  char buf[IOBUFSIZE];
  size_t n;
  int i;
  
  snprintf (buf, sizeof buf, "%3d", code);
  for (i = 0; av[i]; i++)
    {
      n = snprintf (buf, sizeof(buf), "%3d%c%s\r\n",
		    code, av[i+1] ? '-' : ' ', av[i]);
      smtp_timer_enqueue (pthread_self (), SMTP_OUT);      
      if (iobase_writeln (smtp->iob, buf, n) < 0)
	{
	  logger (LOG_ERR, "iobase_writeln: %s", iobase_strerror (smtp->iob));
	  pthread_exit (NULL);
	}
      smtp_timer_dequeue (pthread_self ());      
      logger (LOG_DEBUG, "S: %.*s", n-2, buf);
    }      
}

static void
smtp_reset (struct smtp *smtp, int state)
{
  switch (state)
    {
    case STATE_INIT:
      free (smtp->helo);
      smtp->helo = NULL;
      /* FALL THROUGH */
    case STATE_MAIL:
      free (smtp->sender);
      smtp->sender = NULL;
      /* FALL THROUGH */
    case STATE_RCPT:
      {
	int i;
	for (i = 0; i < smtp->nrcpt; i++)
	  free (smtp->rcpt[i]);
	smtp->nrcpt = 0;
      }
      /* FALL THROUGH */
    case STATE_DATA:
      //FIXME: Clean up any collected mail?
      break;
    }
}  

void
smtp_end (struct smtp *smtp)
{
  smtp_io_send (smtp, 221, "Bye");
}

void
smtp_free (struct smtp *smtp)
{
  iobase_close (smtp->iob);
  free (smtp);
}

enum smtp_keyword
  {
    KW_HELP,
    KW_RSET,
    KW_EHLO,
    KW_HELO,
    KW_MAIL,
    KW_RCPT,
    KW_DATA,
    KW_STARTTLS,
    KW_QUIT,
    MAX_KW
  };

static char *smtp_keyword_trans[MAX_KW] = {
  [KW_HELP] = "HELP",
  [KW_RSET] = "RSET",
  [KW_EHLO] = "EHLO",
  [KW_HELO] = "HELO",
  [KW_MAIL] = "MAIL",
  [KW_RCPT] = "RCPT",
  [KW_DATA] = "DATA",
  [KW_STARTTLS] = "STARTTLS",
  [KW_QUIT] = "QUIT"
};

static int
smtp_keyword_find (char const *kw)
{
  int i;
  for (i = 0; i < MAX_KW; i++)
    if (strcasecmp (kw, smtp_keyword_trans[i]) == 0)
      return i;
  return -1;
}

static int
smtp_help (struct smtp *smtp)
{
  smtp_io_send (smtp, 214, "http://www.ietf.org/rfc/rfc2821.txt");
  return 0;
}

static int
smtp_rset (struct smtp *smtp)
{
  if (smtp->arg)
    {
      smtp_io_send (smtp, 501, "rset does not take arguments");
      return -1;
    }
  smtp_io_send (smtp, 250, "Reset state");

  smtp_reset (smtp, STATE_INIT);

  return 0;
}

static int
smtp_ehlo (struct smtp *smtp)
{
  char const *capa[MAX_CAPA+2];
  int i, j;
  
  if (!smtp->arg)
    {
      smtp_io_send (smtp, 501, "ehlo requires domain address");
      return -1;
    }

  capa[0] = "localhost Mock MTA pleased to meet you";
  for (i = 0, j = 1; i < MAX_CAPA; i++)
    if (!(smtp->capa_mask & CAPA_MASK (i)))
      capa[j++] = capa_str[i];
  capa[j] = NULL;
  smtp_io_mlsend (smtp, 250, capa);

  smtp_reset (smtp, STATE_INIT);
  if ((smtp->helo = strdup (smtp->arg)) == NULL)
    nomemory ();
  return 0;
}

static int
smtp_starttls (struct smtp *smtp)
{
  struct io2 *orig_iob = (struct io2 *) smtp->iob;
  struct iofile *inb = (struct iofile *)orig_iob->iob[IO2_RD];
  struct iofile *outb = (struct iofile *)orig_iob->iob[IO2_WR];
  struct iobase *iob;

  if (smtp->arg)
    {
      smtp_io_send (smtp, 501, "Syntax error (no parameters allowed)");
      return -1;
    }
  
  smtp_io_send (smtp, 220, "Ready to start TLS");
  
  iob = iotls_create (inb->fd, outb->fd);

  if (iob)
    {
      free (inb);
      free (outb);
      free (orig_iob);
      smtp->iob = iob;
      disable_starttls ();
      smtp->capa_mask |= CAPA_MASK (CAPA_STARTTLS);
    }
  else
    {
      free (iob);
      pthread_exit (NULL);
    }
  return 0;
}

static int
smtp_quit (struct smtp *smtp)
{
  return 0;
}

static int
smtp_helo (struct smtp *smtp)
{
  if (!smtp->arg)
    {
      smtp_io_send (smtp, 501, "helo requires domain address");
      return -1;
    }
  smtp_io_send (smtp, 250, "localhost Mock MTA pleased to meet you");

  smtp_reset (smtp, STATE_INIT);
  if ((smtp->helo = strdup (smtp->arg)) == NULL)
    nomemory ();
  return 0;  
}

static int
smtp_mail (struct smtp *smtp)
{
  static char from_str[] = "FROM:";
  static size_t from_len = sizeof(from_str) - 1;
  char *p;
  
  if (!smtp->arg)
    {
      smtp_io_send (smtp, 501, "mail requires email address");
      return -1;
    }
  if (strncasecmp (smtp->arg, from_str, from_len))
    {
      smtp_io_send (smtp, 501, "syntax error");
      return -1;
    }
  p = smtp->arg + from_len;
  while (*p && (*p == ' ' || *p == '\t'))
    p++;
  if (!*p)
    {
      smtp_io_send (smtp, 501, "mail requires email address");
      return -1;
    }
  smtp_reset (smtp, STATE_MAIL);
  if ((smtp->sender = strdup (p)) == NULL)
    nomemory ();
  smtp_io_send (smtp, 250, "Sender ok");
  return 0;
}

static int
smtp_rcpt (struct smtp *smtp)
{
  static char to_str[] = "TO:";
  static size_t to_len = sizeof (to_str) - 1;
  char *p;
  
  if (!smtp->arg)
    {
      smtp_io_send (smtp, 501, "rcpt requires email address");
      return -1;
    }
  if (strncasecmp (smtp->arg, to_str, to_len))
    {
      smtp_io_send (smtp, 501, "syntax error");
      return -1;
    }
  p = smtp->arg + to_len;
  while (*p && (*p == ' ' || *p == '\t'))
    p++;
  if (!*p)
    {
      smtp_io_send (smtp, 501, "to requires email address");
      return -1;
    }
  if (smtp->nrcpt == MAX_RCPT)
    {
      smtp_io_send (smtp, 501, "too many recipients");
      return -1;
    }
  if ((smtp->rcpt[smtp->nrcpt] = strdup (p)) == NULL)
    nomemory ();
  smtp->nrcpt++;
  smtp_io_send (smtp, 250, "Recipient ok");
  return 0;
}

static int
begins_with_from (char const *p)
{
  while (*p == '>')
    p++;
  return strncmp (p, "From", 4) == 0;
}

static int
mailbox_append (FILE *tf)
{
  int fd;
  FILE *fp;
  struct flock lk;
  off_t length;
  int res = 0;
  int c;
  
  /* Open the mailbox */
  fd = open (mailbox_name, O_CREAT|O_WRONLY|O_APPEND, 0600);
  if (fd == -1)
    {
      logger (LOG_ERR, "can't open %s: %s", mailbox_name, strerror (errno));
      return -1;
    }
  lk.l_type = F_WRLCK;
  lk.l_whence = SEEK_END;
  lk.l_start = 0;
  lk.l_len = 0;
  if (fcntl (fd, F_SETLKW, &lk)) /* FIXME: ttl */
    {
      logger (LOG_ERR, "can't lock %s: %s", mailbox_name, strerror (errno));
      close (fd);
      return -1;
    }

  length = lseek (fd, 0, SEEK_END);
  
  fp = fdopen (fd, "a");
  if (!fp)
    {
      logger (LOG_ERR, "fdopen: %s", strerror (errno));
      close (fd);
      return -1;
    }

  while ((c = fgetc (tf)) != EOF)
    fputc (c, fp);
  
  if (ferror (fp) || ferror (tf))
    {
      res = -1;
      fflush (fp);
      ftruncate (fd, length);
    }
  
  lk.l_type = F_UNLCK;
  lk.l_whence = SEEK_SET;
  lk.l_start = 0;
  lk.l_len = 0;
  if (fcntl (fd, F_SETLK, &lk))
    logger (LOG_ERR, "can't unlock %s: %m", mailbox_name);
  fclose (fp);

  return res;
}

static void
tempfile_cleanup (void *ptr)
{
  fclose ((FILE*)ptr);
}

static int
smtp_data (struct smtp *smtp)
{
  char template[] = "/tmp/mockmta.XXXXXX";
  int fd;
  FILE *fp;
  ssize_t n;
  time_t t;
  int res = 0;
  int in_body = 0;
  
  fd = mkstemp (template);
  if (fd == -1)
    {
      logger (LOG_ERR, "can't create temporary: %m");
      smtp_io_send (smtp, 451, "Local filesystem error");
      return -1;
    }

  fp = fdopen (fd, "w+");
  if (!fp)
    {
      logger (LOG_ERR, "fdopen: %m");
      smtp_io_send (smtp, 451, "Local filesystem error");
      close (fd);
      return -1;
    }

  unlink (template);

  pthread_cleanup_push (tempfile_cleanup, fp);
  smtp_io_send (smtp, 354,
		"Enter mail, end with \".\" on a line by itself");

  t = time (NULL);
  fprintf (fp, "From %s %s", smtp->sender, asctime (gmtime (&t)));
  
  while (1)
    {
      char *p;
      
      n = smtp_io_readln (smtp);
      
      if (n <= 0)
	{
	  smtp->state = STATE_QUIT;
	  if (smtp->iob->iob_eof)
	    logger (LOG_ERR, "unexpected end of file");
	  else
	    logger (LOG_ERR, "read error: %s",
		    strerror (smtp->iob->iob_errno));
	  res = 1;
	  break;
	}
      
      if (smtp->buf[n-1] == '\n')
	{
	  if (n > 1 && smtp->buf[n-2] == '\r')
	    {
	      smtp->buf[n-2] = '\n';
	      smtp->buf[n-1] = 0;
	      n--;
	    }
	}
      else
	{
	  logger (LOG_ERR, "line too long");
	  break;
	}

      if (n == 1 && !in_body)
	in_body = 1;
      
      p = smtp->buf;
      if (in_body)
	{
	  if (*p == '.')
	    {
	      if (p[1] == '\n')
		break;
	      if (p[1] == '.')
		{
		  p++;
		  n--;
		}
	    }
	  else if (begins_with_from (p))
	    fputc ('>', fp);
	}
      fwrite (p, n, 1, fp);
    }
  fputc ('\n', fp);

  if (res == 0)
    {
      rewind (fp);
      res = mailbox_append (fp);
    }
  
  pthread_cleanup_pop (1);

  if (res)
    {
      if (smtp->nrcpt > 1)
	logger (LOG_INFO, "%s => %s (%d recipients) failed",
		smtp->sender, smtp->rcpt[0], smtp->nrcpt);
      else
	logger (LOG_INFO, "%s => %s failed",
		smtp->sender, smtp->rcpt[0]);
      smtp_io_send (smtp, 451, "Local filesystem error");
    }
  else
    {
      if (smtp->nrcpt > 1)
	logger (LOG_INFO, "%s => %s (%d recipients) delivered",
		smtp->sender, smtp->rcpt[0], smtp->nrcpt);
      else
	logger (LOG_INFO, "%s => %s delivered",
		smtp->sender, smtp->rcpt[0]);
      smtp_io_send (smtp, 250, "%s Message accepted for delivery", smtp->sid);
      smtp->delivered++;
    }
  
  return res;
}


struct smtp_transition
{
  int new_state;
  int (*handler) (struct smtp *);
};

static struct smtp_transition smtp_transition_table[MAX_STATE][MAX_KW] = {
  [STATE_INIT] = {
    [KW_HELP] = { STATE_INIT, smtp_help },
    [KW_RSET] = { STATE_INIT, smtp_rset },
    [KW_HELO] = { STATE_EHLO, smtp_helo },
    [KW_EHLO] = { STATE_EHLO, smtp_ehlo },
    [KW_QUIT] = { STATE_QUIT, smtp_quit }
  },
  [STATE_EHLO] = {
    [KW_HELP] = { STATE_EHLO, smtp_help },
    [KW_RSET] = { STATE_INIT, smtp_rset },
    [KW_HELO] = { STATE_EHLO, smtp_helo },
    [KW_EHLO] = { STATE_EHLO, smtp_ehlo },
    [KW_MAIL] = { STATE_MAIL, smtp_mail },
    [KW_STARTTLS] = { STATE_EHLO, smtp_starttls },
    [KW_QUIT] = { STATE_QUIT, smtp_quit }
  },
  [STATE_MAIL] = {
    [KW_HELP] = { STATE_MAIL, smtp_help },
    [KW_RSET] = { STATE_INIT, smtp_rset },
    [KW_RCPT] = { STATE_RCPT, smtp_rcpt },
    [KW_HELO] = { STATE_EHLO, smtp_helo },
    [KW_EHLO] = { STATE_EHLO, smtp_ehlo },
    [KW_QUIT] = { STATE_QUIT, smtp_quit }
  },
  [STATE_RCPT] = {
    [KW_HELP] = { STATE_RCPT, smtp_help },
    [KW_RSET] = { STATE_INIT, smtp_rset },
    [KW_RCPT] = { STATE_RCPT, smtp_rcpt },
    [KW_HELO] = { STATE_EHLO, smtp_helo },
    [KW_EHLO] = { STATE_EHLO, smtp_ehlo },
    [KW_DATA] = { STATE_EHLO, smtp_data },
    [KW_QUIT] = { STATE_QUIT, smtp_quit }
  },
};  

static void
disable_starttls (void)
{
#ifdef WITH_TLS
  tls_cert = tls_key = tls_cafile = NULL;
#endif
  smtp_transition_table[STATE_EHLO][KW_STARTTLS].new_state = STATE_ERR;
}

static void
do_smtp (struct smtp *smtp)
{
  struct smtp_transition *trans;

  logger (LOG_DEBUG, "session started");
  smtp_io_send (smtp, 220, "Ready");
  while (smtp->state != STATE_QUIT)
    {
      size_t i;
      int kw;
      int new_state;
      ssize_t n = smtp_io_readln (smtp); 
      if (n <= 0)
	break;
      smtp->buf[--n] = 0;
      logger (LOG_DEBUG, "C: %s", smtp->buf);
      i = strcspn (smtp->buf, " \t");
      if (smtp->buf[i])
	{
	  smtp->buf[i++] = 0;
	  while (i < n && (smtp->buf[i] == ' ' || smtp->buf[i] == '\t'))
	    i++;
	  if (smtp->buf[i])
	    smtp->arg = &smtp->buf[i];
	  else
	    smtp->arg = NULL;
	}
      else
	smtp->arg = NULL;

      kw = smtp_keyword_find (smtp->buf);
      if (kw == -1)
	{
	  smtp_io_send (smtp, 500, "Command unrecognized");
	  continue;
	}

      trans = &smtp_transition_table[smtp->state][kw];
      new_state = trans->new_state;
      if (new_state == STATE_ERR)
	{
	  smtp_io_send (smtp, 500, "Command not valid");
	  continue;
	}

      if (trans->handler (smtp))
	continue;

      smtp->state = new_state;
    }
  if (!smtp->delivered)
    logger (LOG_NOTICE, "session ended without delivery");
  logger (LOG_DEBUG, "session finished");
  smtp_end (smtp);
}

struct smtp_timer
{
  pthread_t tid;
  int state;
  struct timespec wakeup_time;
  struct smtp_timer *prev, *next;
};

struct smtp_timer *smtp_timer_head, *smtp_timer_tail;
static pthread_mutex_t smtp_timer_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t smtp_timer_cond = PTHREAD_COND_INITIALIZER;

static inline int
timespec_cmp (struct timespec const *a, struct timespec const *b)
{
  if (a->tv_sec < b->tv_sec)
    return -1;
  if (a->tv_sec > b->tv_sec)
    return 1;
  if (a->tv_nsec < b->tv_nsec)
    return -1;
  if (a->tv_nsec > b->tv_nsec)
    return 1;
  return 0;
}

void
smtp_timer_enqueue (pthread_t tid, int state)
{
  struct smtp_timer *timer;

  timer = malloc (sizeof (timer[0]));
  if (!timer)
    nomemory ();
  timer->tid = tid;
  timer->state = state;
  clock_gettime (CLOCK_REALTIME, &timer->wakeup_time);
  timer->wakeup_time.tv_sec += smtp_timeout;
  
  pthread_mutex_lock (&smtp_timer_mutex);
  timer->next = NULL;
  timer->prev = smtp_timer_tail;
  if (smtp_timer_tail)
    smtp_timer_tail->next = timer;
  else
    smtp_timer_head = timer;
  smtp_timer_tail = timer;
  if (timer == smtp_timer_head)
    pthread_cond_broadcast (&smtp_timer_cond);	
  pthread_mutex_unlock (&smtp_timer_mutex);
}

void
smtp_timer_unlink (struct smtp_timer *timer)
{
  if (timer->prev)
    timer->prev->next = timer->next;
  else
    smtp_timer_head = timer->next;
  
  if (timer->next)
    timer->next->prev = timer->prev;
  else
    smtp_timer_tail = timer->prev;
}
  
void
smtp_timer_dequeue (pthread_t tid)
{
  struct smtp_timer *p;
  pthread_mutex_lock (&smtp_timer_mutex);
  for (p = smtp_timer_head; p; p = p->next)
    {
      if (p->tid == tid)
	{
	  smtp_timer_unlink (p);
	  free (p);
	  break;
	}
    }
  pthread_mutex_unlock (&smtp_timer_mutex);
}

void *
thr_watcher (void *ptr)
{
  pthread_mutex_lock (&smtp_timer_mutex);
  while (1)
    {
      struct smtp_timer *timer = smtp_timer_head;
      
      if (!timer)
	{
	  pthread_cond_wait (&smtp_timer_cond, &smtp_timer_mutex);
	  continue;
	}

      switch (pthread_cond_timedwait (&smtp_timer_cond,
				      &smtp_timer_mutex,
				      &timer->wakeup_time))
	{
	case 0:
	  /* Condition signalled: timer list has been updated.  Restart. */
	  continue;

	case ETIMEDOUT:
	  /* Wakeup time is reached */
	  break;

	default:
	  /* Should not happen */
	  logger (LOG_ERR, "unexpected error from pthread_cond_timedwait: %m");
	  exit (EX_FAILURE);
	}

      if (timer == smtp_timer_head)
	{
	  /* Thread I/O timed out.  Terminate the thread. */
	  pthread_cancel (timer->tid);
	  smtp_timer_unlink (timer);
	  free (timer);
	}
    }
  pthread_mutex_unlock (&smtp_timer_mutex);
  return NULL;
}

int
tcpwrap_access(int fd)
{
#ifdef WITH_LIBWRAP
  struct request_info req;

  request_init (&req, RQ_DAEMON, PACKAGE_NAME, RQ_FILE, fd, NULL);
  fromhost (&req);
  return hosts_access (&req);
#else
  return 1;
#endif
}

static int
mta_open (struct addrinfo *ap)
{
  int on = 1;
  int fd;

  fd = socket (PF_INET, SOCK_STREAM, 0);
  if (fd < 0)
    {
      logger (LOG_ERR, "socket: %m");
      exit (EX_FAILURE);
    }

  setsockopt (fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof (on));

  if (bind (fd, ap->ai_addr, ap->ai_addrlen) < 0)
    {
      close (fd);
      logger (LOG_ERR, "bind: %m");
      exit (EX_FAILURE);
    }
  
  listen (fd, 5);

  return fd;
}

static void
smtp_cleanup (void *ptr)
{
  struct smtp *smtp = ptr;
  if (smtp->state != STATE_QUIT)
    logger (LOG_NOTICE, "session timed out (%s)", state_str[smtp->state]);
  smtp_free (smtp);
}

void *
thr_smtp (void *ptr)
{
  struct smtp *smtp = ptr;
  struct session_data *sd = priv_get_session_data ();
  memcpy (sd->sid, smtp->sid, sizeof (sd->sid));
  pthread_cleanup_push (smtp_cleanup, ptr);
  do_smtp (ptr);
  pthread_cleanup_pop (1);
  return NULL;
}

static void
smtp_tempfail (void *ptr)
{
  exit (EX_TEMPFAIL);
}

void *
thr_smtp_stdio (void *ptr)
{
  pthread_cleanup_push (smtp_tempfail, ptr);
  thr_smtp (ptr);
  pthread_cleanup_pop (0);
  exit (EX_OK);
  return NULL;
}

#define MAX_HOST_NAME 256

void *
thr_mta_listener (void *ptr)
{
  int fd = *(int*) ptr;
  pthread_attr_t attr;

  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
  
  while (1)
    {
      int sfd;
      struct sockaddr_storage remote_addr;
      socklen_t len = sizeof (remote_addr);
      char *s;
      
      if ((sfd = accept (fd, (struct sockaddr *) &remote_addr, &len)) < 0)
	{
	  logger (LOG_ERR, "accept: %m");
	  exit (EX_FAILURE);
	}

      s = sockaddr_str ((struct sockaddr *) &remote_addr, len);
      if (!tcpwrap_access (sfd))
	{
	  logger (LOG_NOTICE, "denied access from %s", s);
	  close (sfd);
	}
      else
	{
	  pthread_t tid;
	  struct smtp *smtp = smtp_create (sfd, sfd);
	  logger (LOG_INFO, "%s: connect from %s", smtp->sid, s);
	  pthread_create (&tid, &attr, thr_smtp, smtp);
	}
      
      free (s);
    }
  return NULL;
}

static int fatal_signals[] = {
  SIGHUP,
  SIGINT,
  SIGQUIT,
  SIGTERM,
  0
};

static void
signull(int sig)
{
}

static void
set_log_level (char const *arg)
{
  static char *logstr[] = {
    [LOG_EMERG]   = "emerg",
    [LOG_ALERT]   = "alert",
    [LOG_CRIT]    = "crit",
    [LOG_ERR]     = "err",
    [LOG_WARNING] = "warning",
    [LOG_NOTICE]  = "notice",
    [LOG_INFO]    = "info",
    [LOG_DEBUG]   = "debug",
    NULL
  };
  int i;

  for (i = 0; logstr[i]; i++)
    if (strcmp (logstr[i], arg) == 0)
      {
	log_level = i;
	return;
      }

  logger (LOG_CRIT, "invalid log level: %s", arg);
  exit (EX_USAGE);
}

static char usage_help[] = "Usage: mockmta [OPTIONS] MAILBOX";
static char prog_descr[] = "Simple MTA for testing purposes";
static char *opthelp[] = {
#ifdef WITH_TLS  
  "-a CA               name of certificate authority file",
  "-c CERT             name of the certificate file",
#endif  
  "-d                  daemon mode",
  "-f                  remain in foreground (implies -d)",
#ifdef WITH_TLS  
  "-k KEY              name of the certificate key file",
#endif  
  "-l LEVEL            log events at the specified, or more important,",
  "                    loglevels",
  "-P FILE             write PID to FILE (with -d or -f)",
  "-p [ADDRESS:]PORT   listen on this port",
  "-s                  always log via syslog",
  "-t SEC              set SMTP timeout",
  "-v                  print program version and exit",
  "-?                  print this help summary and exit",
  NULL
};

static void
usage (FILE *fp)
{
  int i;

  fprintf (fp, "%s\n", usage_help);
  fprintf (fp, "%s\n", prog_descr);
  fprintf (fp, "\nOPTIONS are:\n\n");
  for (i = 0; opthelp[i]; i++)
    fprintf (fp, "    %s\n", opthelp[i]);
  fputc ('\n', fp);
  fprintf (fp, "Report bugs to <%s>\n", PACKAGE_BUGREPORT);
  fprintf (fp, "%s home page: <%s>\n", PACKAGE_NAME, PACKAGE_URL);
}

static int copyright_year = 2021;

static void
version (void)
{
  printf ("%s (%s) %s\n", progname, PACKAGE_NAME, PACKAGE_VERSION);
  printf ("Copyright (C) %d Sergey Poznyakoff\n", copyright_year);
  printf ("\
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\nThis is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n\
");
#ifdef WITH_TLS
  printf ("\n");
  printf ("Using GnuTLS %s\n", gnutls_check_version (NULL));
#endif
#ifdef WITH_LIBWRAP
  printf ("Using libwrap\n");
#endif  
}

int
main (int argc, char **argv)
{
  int c;
  int fd;
  int foreground = 0;
  int syslog_opt = 0;
  struct sigaction act;
  sigset_t sigs;
  int i;
  pthread_t tid;
  
  progname = argv[0];
  
  while ((c = getopt (argc, argv, "a:dc:fk:l:P:p:st:v?")) != EOF)
    {
      switch (c)
	{
	case 'd':
	  daemon_opt = 1;
	  break;

	case 'f':
	  daemon_opt = 1;
	  foreground = 1;
	  break;

	case 'l':
	  set_log_level (optarg);
	  break;

	case 'P':
	  pidfile = optarg;
	  break;
	  
	case 'p':
	  portstr = optarg;
	  break;

	case 's':
	  syslog_opt = 1;
	  break;
	  
	case 't':
	  smtp_timeout = atoi (optarg);
	  if (smtp_timeout <= 0)
	    {
	      logger (LOG_ERR, "invalid timeout value");
	      exit (EX_USAGE);
	    }
	  break;

	case 'v':
	  version ();
	  exit (EX_OK);
	  
	default:
	  if (optopt == '?')
	    {
	      usage (stdout);
	      exit (EX_OK);
	    }
	  else if (set_tls_opt (c))
	    {
	      usage (stderr);
	      exit (EX_USAGE);
	    }
	}
    }

  argc -= optind;
  argv += optind;

  if (argc != 1)
    {
      usage (stderr);
      exit (EX_USAGE);
    }

  mailbox_name = argv[0];

  if (tls_init ())
    {
      disable_starttls ();
    }

  /* Set up signal handling */
  sigemptyset (&sigs);
      
  act.sa_flags = 0;
  sigemptyset (&act.sa_mask);
  act.sa_handler = signull;
      
  for (i = 0; fatal_signals[i]; i++)
    {
      sigaddset (&sigs, fatal_signals[i]);
      sigaction (fatal_signals[i], &act, NULL);
    }
  sigaddset (&sigs, SIGPIPE);
  sigaddset (&sigs, SIGALRM);
  sigaddset (&sigs, SIGCHLD);
  pthread_sigmask (SIG_BLOCK, &sigs, NULL);

  if (daemon_opt)
    {
      struct addrinfo *addr = address_parse (portstr);
      fd = mta_open (addr);
      freeaddrinfo (addr);

      if (!foreground)
	{
	  switch (fork ())
	    {
	    case -1:
	      logger (LOG_ERR, "daemon: %m");
	      exit (EX_FAILURE);
	      
	    case 0:
	      break;
	      
	    default:
	      _exit (0);
	    }
	  
	  if (setsid () == -1)
	    {
	      logger (LOG_ERR, "setsid: %m");
	      exit (EX_FAILURE);
	    }

	  chdir ("/");

	  close (0);
	  close (1);
	  close (2);
	  open (_PATH_DEVNULL, O_RDONLY);
	  open (_PATH_DEVNULL, O_WRONLY);
	  dup (1);
      
	  /* Set up logging */
	  openlog (progname, LOG_PID, LOG_MAIL);
	  logger = log_syslog;

	  if (pidfile)
	    {
	      FILE *fp = fopen (pidfile, "w");
	      if (fp)
		{
		  fprintf (fp, "%lu\n", (unsigned long) getpid());
		  fclose (fp);
		}
	      else
		{
		  logger (LOG_ERR, "can't open pidfile %s for writing: %m",
			  pidfile);
		  pidfile = NULL;
		}
	    }
	}
      else if (syslog_opt)
	{
	  openlog (progname, LOG_PID, LOG_MAIL);
	  logger = log_syslog;
	}
      
      pthread_create (&tid, NULL, thr_watcher, NULL);
      pthread_create (&tid, NULL, thr_mta_listener, &fd);      
    }
  else
    {
      struct sockaddr_storage remote_addr;
      socklen_t len = sizeof (remote_addr);
      struct smtp *smtp = smtp_create (0, 1);
      char *s = NULL;

      if (syslog_opt)
	{
	  openlog (progname, LOG_PID, LOG_MAIL);
	  logger = log_syslog;
	}

      if (getpeername (0, (struct sockaddr *) &remote_addr, &len) == 0)
	{
	  s = sockaddr_str ((struct sockaddr *) &remote_addr, len);

	  if (!tcpwrap_access (fd))
	    {
	      logger (LOG_NOTICE, "denied access from %s", s);
	      exit (EX_FAILURE);
	    }
	}

      if (s)
	{
	  logger (LOG_INFO, "%s: connect from %s", smtp->sid, s);
	  free (s);
	}
      pthread_create (&tid, NULL, thr_watcher, NULL);
      pthread_create (&tid, NULL, thr_smtp_stdio, smtp);
      pthread_detach (tid);
    }

  /* Unblock only the fatal signals */
  sigemptyset (&sigs);
  for (i = 0; fatal_signals[i]; i++)
    sigaddset (&sigs, fatal_signals[i]);
  
  pthread_sigmask (SIG_UNBLOCK, &sigs, NULL);
  
  /* Wait for signal to arrive */
  sigwait (&sigs, &i);

  if (pidfile)
    unlink (pidfile);

  exit (EX_OK);
}
